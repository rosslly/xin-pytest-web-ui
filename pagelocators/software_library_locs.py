# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By


class SoftwareLibraryLocator:
    """软件库模块"""
    #软件库元素
    softwarelibrary_loc = (By.XPATH, '/html/body/div[3]/div[1]/div/div/div[1]/ul/li[6]/a')
    #软件分类元素
    software_class_loc = (By.XPATH, '/html/body/div[5]/div[3]/div/div[2]/div/ul/li[2]/a')
    #数据存储元素
    data_storage_loc = (By.XPATH, '//*[@id="mainScreen"]/div/div/div/div[1]/div/div[1]/div[2]/div[1]/a')
    #首页元素
    top_page_loc = (By.XPATH, '/html/body/div[5]/div[2]/div/div/div[1]/div/div[1]/div/ul/li[1]/a')
