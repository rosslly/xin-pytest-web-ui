import json
json_str = '{"a": "aa", "b": "bb", "c": {"d": "dd", "e": "ee"}}'
target = {}
for k, v in json.loads(json_str).items():
    if isinstance(v, str):
         target[k] = v
    else:
        for k1, v1 in v.items():
            target[k1] = v1
print(target)
# {'a': 'aa', 'b': 'bb', 'd': 'dd', 'e': 'ee'}